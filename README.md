# s2i builder for sphinx documentation

Source: [https://gitlab.com/kraxel/s2i-sphinx](https://gitlab.com/kraxel/s2i-sphinx)

Image: registry.gitlab.com/kraxel/s2i-sphinx

## Deploy in openshift

```
oc new-app registry.gitlab.com/kraxel/s2i-sphinx~git://some.host/path/to/linux/kernel/source/repo.git
```

